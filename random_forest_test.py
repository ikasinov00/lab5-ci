import numpy as np
import pandas as pd
from random import randrange
import main
import random


point1 = abs(np.random.normal(1, 12, 150))
point2 = abs(np.random.normal(2, 8, 150))
point3 = abs(np.random.normal(3, 2, 150))
point4 = abs(np.random.normal(10, 15, 150))
classes = []
for i in range(0, 150):
    classes.append(randrange(3))

data = pd.DataFrame()

data['col1'] = point1
data['col2'] = point2
data['col3'] = point3
data['col4'] = point4
data['CLASS'] = classes

assert main.random_forest(data) > 0.3
