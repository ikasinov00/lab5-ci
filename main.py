import sklearn
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split


def random_forest(data):
    X = data.drop('CLASS', axis=1)
    y = data['CLASS']

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.6, random_state=2)

    rfc = RandomForestClassifier()
    rfc.fit(X_train, y_train)
    predicted = rfc.predict(X)

    predicted_file = open('predict.csv', 'w')
    predicted_file.write('predicted,actual\n')

    for i, j in zip(predicted, y.tolist()):
        predicted_file.write(str(i) + ',' + str(j) + '\n')

    return sklearn.metrics.accuracy_score(y, predicted)
